
#-----------------------------------------------------------------------
#
#  Copyright c 1999-2013, Stratasys Inc.
#  All Rights Reserved Worldwide.
#
#  %full_name:		1/tcl/schooner.tcl/1.1.5 %
#  %derived_by:		tstudans %
#  %date_created:	Thu Dec 19 12:56:48 2013 %
#
#  Script to define schooner-specific data and procedures. This script
#  should be executed at global scope.
#
#  Procedures
#    schoonerMatl   -- return list of possible main or alternate
#		       materials
#    schoonerTips    -- return list of possible main or alternate
#		       tip sizes
#    schoonerSlices  -- return list of possible slice-heights
#
#  Data
#    schooner:envelope   -- final part size envelope dimensions
#    schooner:buildLimit -- maximum size of build envelope
#    schooner:travHeight -- traverse height
#    schooner:packGutterX -- air gap gutter when packing
#    schooner:packGutterY -- air gap gutter when packing
#
#-----------------------------------------------------------------------

    ######################################
    #
    #   schooner-specific data. All data is in inches.
    #

    # restrict slice heights to those in lister only
varedit sliceHeight -allowarbitrary false;

    # displayed envelope - max size of final part after shrinkage
    # These values must be smaller than the buildLimit values
    # to accommodate material shrinkage, support growth and
    # base oversize.
global schooner:envelope;
if { [info exists schooner:envelope] == 0 } {
    defvar schooner:envelope {
	-type	    point3
	-label	    "Envelope dimensions"
	-default    { 8.0 8.0 12.0 }
	# try to raise the maximum build size. Place a small cube at all edges, print it, measure the free space and raise the limits. Then print again and check if it works.
	-readonly   true
    };
}
    # physical limits of the modeler
global schooner:buildLimit;
if { [info exists schooner:buildLimit] == 0 } {
    defvar schooner:buildLimit {
	-type	    point3
	-label	    "Envelope build limit"
	-default    { 8.30 8.30 12.50 }
	# when the limis at "Envelope dimensions" where increased these limits need to be increased too. Only 0.005 more then the "Envelope dimensions" is enough
	-readonly   true
    };
}
global schooner:travHeight;
if { [info exists schooner:travHeight] == 0 } {
    defvar schooner:travHeight {
	-type	    length
	-label	    "Traverse height"
	-default    0.026
	-readonly   true
    };
}

global schooner:packGutterX;
if { [info exists schooner:packGutterX] == 0 } {
    defvar schooner:packGutterX { 
    	-type		length 
    	-default	0.1 
		# min. distance between the parts, reduce it to 0.05 or smaler
    	-label		"Gutter X" 
    	-balloon	"Gutter in X dir between jobs when packing" 
    	-min		0.0 
    	-max            2.0 
    }; 
};
global schooner:packGutterY;
if { [info exists schooner:packGutterY] == 0 } {
    defvar schooner:packGutterY { 
    	-type		length 
    	-default	0.1
		# min. distance between the parts, reduce it to 0.05 or smaler
    	-label		"Gutter Y" 
    	-balloon	"Gutter in Y dir between jobs when packing" 
    	-min		0.0 
    	-max            2.0 
    }; 
};
global schooner:timeEstFactor;
if { [info exists schooner:timeEstFactor] == 0 } {
    defvar schooner:timeEstFactor {
    	-type		real
	-default	1.00
	-label		"Time estimate factor"
	-balloon	"Time estimate multiplier"
	-min		0.0
	-max		100.0
    };
};
    # interface controls
set ::autoCenterToolpath false;		# auto-center in packing?
set ::allowVWC           true;		# allow variable width contour?


########################################################################
#
#   PRIVATE
#	Returns a list of possible main or alternate materials, or
#	the name of the default main or alternate material. There
#	are two main forms for calling this function:
#
#	    schoonerMatl -main
#	    schoonerMatl -alt $mainType
#
#	where "mainType" would be a variable indicating the current
#	choice for main material.
#
#	Optionally, the keyword "-default" can be appended on the end
#	of the argument list to make the function return the default
#	main or alternate material.
#
#   RETURNS
#	When returning a material list, the list returned consists
#	of pairs where the first value is the mnemonic name of the
#	material, and the second value is the user-displayable
#	description.
#
#	When returning the default material, just the mnemonic name
#	is returned.
#
########################################################################
proc	schoonerMatl { matl { arg2 "" } { arg3 "" } } {

					# main material list
    if { $matl == "-main" } {
						# default
	if { $arg2 == "-default" } {
	    return P400;
						# material list
	} else {
	    return { {P400 "ABS P400"} }
	}

					# alternate material list
    } elseif { $matl == "-alt" } {
						# default
	if { $arg3 == "-default" } {
	    return P400sr;
						# material list
	} else {
	    return { {P400sr "ABS P400SR"} }
	}
    }

    errormsg "schoonerMatl: Bad material.";
    return "";
}; # schoonerMatl


########################################################################
#
#   PRIVATE
#	Returns a list of the valid sizes for the tip of the main
#	or alternate material. Tip sizes are formatted "Txx" where
#	xx is the size of the tip in millimeters.
#
#   ARGUMENTS
#	tip		Either "-main" or "-alt".
#	mainMaterial	Mnemonic for the main material type.
#	altMaterial	Mnemonic for the alternate material type.
#	default		Optional argument. If "-default", returns the
#			name of the default tip size.
#
#   RETURNS
#	The list of tip sizes for the material, or the default tip
#	size.
#
########################################################################
proc	schoonerTips { tip mainMaterial altMaterial { default "" } } {
				    # main material tip
    if { $tip == "-main" } {
	if { $default == "-default" } {		# default tip
	    switch -- $mainMaterial {
    		P400 { return T12; }
    		default { return T12; }
	    }
	} else {				# tip list
	    switch -- $mainMaterial {
    		P400 { return { T12 }; }
    		default { return { T12 }; }
	    }
	}
				    # alternate material tip
    } elseif { $tip == "-alt" } {
	if { $default == "-default" } {		# default tip
	    switch -- $altMaterial {
    		P400sr 	{ return T12; }
    		default { return T12; }
	    }
	} else {				# tip list
	    switch -- $altMaterial {
    		P400sr 	{ return { T12 }; }
    		default { return { T12 }; }
	    }
	}
    }

    errormsg "schoonerTips: Bad material.";
    return "";
}; # schoonerTips


########################################################################
#
#   PRIVATE
#	Returns the list of valid slice heights for the material/tip
#	combinations. The heights listed here are ones that are
#	tested, calibrated, and endorsed by Stratasys.
#
#   ARGUMENTS
#	mainMaterial	Mnemonic for the main material type.
#	mainTip		Size for the main material tip.
#	altMaterial	Mnemonic for the alternate material type.
#	altTip		Size for the alternate material tip.
#	request		Optional argument. If "-default", returns the
#			name of the default slice height. If "-limits",
#			returns a two-element list with the minimum and
#			maximum slice heights.
#
#   RETURNS
#	The list of valid slice heights, given the materials and tip
#	sizes, or the default slice height, or the slice height limits.
#
########################################################################
proc	schoonerSlices { mainMaterial mainTip altMaterial altTip {request ""}} {
    if { $request == "-default" } {
	return 0.013;
    } elseif { $request == "-limits" } {
	return {  0.006 0.013 };
    }
    return { 0.006 0.008 0.010 0.013 };
}; # schoonerSlices

#########################################################################
# Return extrusion tip PM limit in hours.
# Return -1 if no limit.
#########################################################################
proc	schoonerExtrusionTipLimit { {mainMaterial ""} } {
    return "-1";
}

#########################################################################
# Return extrusion tip part number.
# Return empty string if not a customer replaceable unit (CRU).
#########################################################################
proc	schoonerExtrusionTipPN { {mainMaterial ""} } {
    return "";
}

#########################################################################
# Return tip cover PM limit in hours.
# Return -1 if no limit.
#########################################################################
proc	schoonerTipCoverLimit { {mainMaterial ""} } {
    return "-1";
}

#########################################################################
# Return tip cover part number.
# Return empty string if not a customer replaceable unit (CRU).
#########################################################################
proc	schoonerTipCoverPN { {mainMaterial ""} } {
    return "";
}

#########################################################################
# Return tip wipe PM limit in hours.
# Return -1 if no limit.
#########################################################################
proc	schoonerTipWipeLimit { {mainMaterial ""} } {
    return "500";
}

#########################################################################
# Return tip wipe part number.
# Return empty string if not a customer replaceable unit (CRU).
#########################################################################
proc	schoonerTipWipePN { {mainMaterial ""} } {
    return "540-00101";
}

#########################################################################
# Return filament present switch PM limit in hours.
# Return -1 if no limit.
#########################################################################
proc    schoonerFilamentSwitchLimit { {mainMaterial ""} } {
    return "-1";
}
