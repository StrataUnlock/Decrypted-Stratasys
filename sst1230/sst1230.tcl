#  CC  NEW T16 
#-----------------------------------------------------------------------
#
#  Copyright c 1999-2007, Stratasys Inc.
#  All Rights Reserved Worldwide.
#
#    %full_name:       1/tcl/sst1230.tcl/1 %
#    %derived_by:      dholzwar %
#    %date_created:    Fri Sep 21 14:35:45 2007 %
#
#
#  $Source: /agrep/puc/apps/common/def/kona/kona.tcl,v $
#  $Author: sharris $
#  $Revision: 1.6 $
#  $Date: 2005/09/14 17:39:05 $
#
#  Script to define sst1230 specific data and procedures. This script
#  should be executed at global scope.
#
#  Procedures
#    sst1230Matl    -- return list of possible main or alternate
#		       materials
#    sst1230Tips    -- return list of possible main or alternate
#		       tip sizes
#    sst1230Slices  -- return list of possible slice-heights
#
#  Data
#    sst1230:envelope   -- final part size envelope dimensions
#    sst1230:buildLimit -- maximum size of build envelope
#    sst1230:travHeight -- traverse height
#    sst1230:packGutterX -- air gap gutter when packing
#    sst1230:packGutterY -- air gap gutter when packing
#
#-----------------------------------------------------------------------

    ######################################
    #
    #   sst1230-specific data. All data is in inches.
    #

    # restrict slice heights to those in lister only
varedit sliceHeight -allowarbitrary false;

    # displayed envelope - max size of final part after shrinkage
    # These values must be smaller than the buildLimit values
    # to accommodate material shrinkage, support growth and
    # base oversize.
global sst1230:envelope;
if { [info exists sst1230:envelope] == 0 } {
    defvar sst1230:envelope {
	-type	    point3
	-label	    "Envelope dimensions"
	-default    { 10.5 10.5 12.40 }              # default { 10.0 10.0 12.0 }
	-readonly   true
    };
}
    # physical buildLimits of the modeler base
global sst1230:buildLimit;
if { [info exists sst1230:buildLimit] == 0 } {
    defvar sst1230:buildLimit {
	-type	    point3
	-label	    "Envelope build limit"
	-default    { 10.59 10.59 12.50 }           # default { 10.30 10.30 12.50 }
	-readonly   true
    };
}
global sst1230:travHeight;
if { [info exists sst1230:travHeight] == 0 } {
    defvar sst1230:travHeight {
	-type	    length
	-label	    "Traverse height"
	-default    0.026
	-readonly   true
    };
}

global sst1230:packGutterX;
if { [info exists sst1230:packGutterX] == 0 } {
    defvar sst1230:packGutterX { 
    	-type		length 
    	-default	0.08 # default 0.25
    	-label		"Gutter X" 
    	-balloon	"Gutter in X dir between jobs when packing" 
    	-min		0.0 
    	-max            2.0 
    }; 
};
global sst1230:packGutterY;
if { [info exists sst1230:packGutterY] == 0 } {
    defvar sst1230:packGutterY { 
    	-type		length 
	-default	0.08 # default 0.25
    	-label		"Gutter Y" 
    	-balloon	"Gutter in Y dir between jobs when packing" 
    	-min		0.0 
    	-max            2.0 
    }; 
};
global sst1230:timeEstFactor;
if { [info exists sst1230:timeEstFactor] == 0 } {
    defvar sst1230:timeEstFactor {
    	-type		real
	-default	1.00
	-label		"Time estimate factor"
	-balloon	"Time estimate multiplier"
	-min		0.0
	-max		100.0
    };
};
    # interface controls
set ::autoCenterToolpath true;		# auto-center in packing?
set ::allowVWC           true;		# allow variable width contour?


########################################################################
#
#   PRIVATE
#	Returns a list of possible main or alternate materials, or
#	the name of the default main or alternate material. There
#	are two main forms for calling this function:
#
#	    sst1230Matl -main
#	    sst1230Matl -alt $mainType
#
#	where "mainType" would be a variable indicating the current
#	choice for main material.
#
#	Optionally, the keyword "-default" can be appended on the end
#	of the argument list to make the function return the default
#	main or alternate material.
#
#   RETURNS
#	When returning a material list, the list returned consists
#	of pairs where the first value is the mnemonic name of the
#	material, and the second value is the user-displayable
#	description.
#
#	When returning the default material, just the mnemonic name
#	is returned.
#
########################################################################
proc	sst1230Matl { matl { arg2 "" } { arg3 "" } } {

					# main material list
    if { $matl == "-main" } {
						# default
	if { $arg2 == "-default" } {
	    return P430;
						# material list
	} else {
	    return { {P430 "ABS P430"} }
	}

					# alternate material list
    } elseif { $matl == "-alt" } {
						# default
	if { $arg3 == "-default" } {
	    return P400sr;
						# material list
	} else {
	    return { {P400sr "ABS P400SR"} }
	}
    }

    errormsg "sst1230Matl: Bad material.";
    return "";
}; # sst1230Matl


########################################################################
#
#   PRIVATE
#	Returns a list of the valid sizes for the tip of the main
#	or alternate material. Tip sizes are formatted "Txx" where
#	xx is the size of the tip in millimeters.
#
#   ARGUMENTS
#	tip		Either "-main" or "-alt".
#	mainMaterial	Mnemonic for the main material type.
#	altMaterial	Mnemonic for the alternate material type.
#	default		Optional argument. If "-default", returns the
#			name of the default tip size.
#
#   RETURNS
#	The list of tip sizes for the material, or the default tip
#	size.
#
########################################################################
proc	sst1230Tips { tip mainMaterial altMaterial { default "" } } {
				    # main material tip
    global modelTip;

    if { $tip == "-main" } {
	if { $default == "-default" } {		# default tip
	    switch -- $mainMaterial {
    		P430 { return T16; }
    		default { return T16; }
	    }
	} else {				# tip list
	    switch -- $mainMaterial {
    		P430 { return { T16 }; }
    		default { return { T16 }; }
	    }
	}
				    # alternate material tip
    } elseif { $tip == "-alt" } {
	if { $default == "-default" } {		# default tip
	    switch -- $altMaterial {
    		P400sr 	{ return T16; }
    		default { return T16; }
	    }
	} else {				# tip list
	    switch -- $altMaterial {
    		P400sr 	{
                    if { $modelTip == "T16" } {
                        return { T16 };
                    } else {
                        return { T16 };
                    }
                }
    		default { return { T16 }; }
	    }
	}
    }

    errormsg "sst1230Tips: Bad material.";
    return "";
}; # sst1230Tips


########################################################################
#
#   PRIVATE
#	Returns the list of valid slice heights for the material/tip
#	combinations. The heights listed here are ones that are
#	tested, calibrated, and endorsed by Stratasys.
#
#   ARGUMENTS
#	mainMaterial	Mnemonic for the main material type.
#	mainTip		Size for the main material tip.
#	altMaterial	Mnemonic for the alternate material type.
#	altTip		Size for the alternate material tip.
#	request		Optional argument. If "-default", returns the
#			name of the default slice height. If "-limits",
#			returns a two-element list with the minimum and
#			maximum slice heights.
#
#   RETURNS
#	The list of valid slice heights, given the materials and tip
#	sizes, or the default slice height, or the slice height limits.
#
########################################################################
proc	sst1230Slices { mainMaterial mainTip altMaterial altTip {request ""}} {
    if { $request == "-default" } {
	return 0.010;
    } elseif { $request == "-limits" } {
	return { 0.006 0.013 }; #default { 0.010 0.013 } -- Insert MIN and MAX slice height
    }
    return { 0.006 0.008 0.010 0.013 }; # add slice heights to be selectable / must be in between MIN and MAX
}; # sst1230Slices
