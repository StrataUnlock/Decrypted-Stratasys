Changes


**Increased the max. build size / volume**
```
    -label	    "Envelope dimensions"
	-default    { **10.5 10.5 12.40** }              *# default { 10.0 10.0 12.0 }*

	-label	    "Envelope build limit"
	-default    { **10.59 10.59 12.50** }           *# default { 10.30 10.30 12.50 }*
```

	
**Reduzed the min. distance between the parts**
```
    global sst1230:packGutterX;
    	-default	0.08 # default 0.25
    	-label		"Gutter X" 

    global sst1230:packGutterY;
        -default	0.08 # default 0.25
        -label		"Gutter Y" 
```

        
**Added lower slice heights**
```
    proc	sst1230Slices { mainMaterial mainTip altMaterial altTip {request ""}} {
        if { $request == "-default" } {
    	return 0.010;
        } elseif { $request == "-limits" } {
    	return { 0.006 0.013 }; #default { 0.010 0.013 } - set MIN and MAX
        }
        return { 0.006 0.008 0.010 0.013 }; # set available slice heights
    }; # sst1230Slices


```
