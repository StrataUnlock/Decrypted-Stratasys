# Decrypted-Stratasys
Decrypted / Unlocked modeler files for Stratasys 3D-Printers to use with Insight up to v11.4 or CatalystEx. 
Best resluts can be achieved with Insight.

###### **Printer Names**

* { "oahu", "Dimension BST 1200" }
* { "kona", "Dimension SST 1200" }
* { "bst1230", "Dimension BST 1200es" }
* { "sst1230", "Dimension SST 1200es" }
* { "solo", "Mojo" }
* { "lanai", "Dimension Elite" }
* { "hana", "uPrint" }
* { "paia", "uPrint Plus" }
* { "kaila", "uPrint SE" }
* { "molokai", "uPrint SE Plus" }
* { "lahaina", "HP Designjet 3D" }
* { "kapalua", "HP Designjet Color 3D" },
* { "dimension", "Dimension/BST/BST 768" },
* { "schooner", "Dimension SST/SST 768" },
* { "mariner", "Prodigy" },
* { "spinnaker", "Prodigy Plus" },
* { "200mc", "Fortus 200mc" },
* { "kapaa", "Fortus 250mc" },
* { "waikiki", "Fortus 450mc" },
* { "vt2l", "Fortus 400mc Large" },
* { "vt2s", "Fortus 400mc Small" },
* { "mc360", "Fortus 360mc Small" },
* { "mc360l", "Fortus 360mc Large" },
* { "kahuku", "Fortus 380mc" },
* { "fox", "FDM Titan" },
* { "titanti", "FDM TitanTI" },
* { "lffs", "Fortus 900mc" },
* { "mercury", "Fortus 900mc sq" },
* { "caesara", "FDM Vantage i" },
* { "caesarx", "FDM Vantage X" },
* { "caesarb", "FDM Vantage S" },
* { "caesarc", "FDM Vantage SE" },
* { "quantum", "FDM Maxum/Quantum" },
* { "connex", "Objet500 Connex" },
* { "dorado1", "F170" },
* { "dorado2", "F270" },
* { "dorado3", "F370" },
* { "hilo", "Fortus 450mc" }
